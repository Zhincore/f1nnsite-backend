const { sanitizeEntity } = require('strapi-utils');

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  async find(ctx) {
    const entities = await strapi.query("outfits").model.collection().query(qb => {
      if (ctx.query.featured) qb.where("featured", ctx.query.featured == "true");
    }).fetch({ withRelated: ["icon.upload_file"] }) || [];

    return entities.map(entity => sanitizeEntity(entity, { model: strapi.models.outfits }));
  },

  async findOne(ctx) {
    const { slug } = ctx.params;

    const entity = await strapi.services.outfits.findOne({ slug });
    return sanitizeEntity(entity, { model: strapi.models.outfits });
  },
};

module.exports = ({ env }) => ({
  host: env('HOST', '127.0.0.1'),
  port: env.int('PORT', 1337),
  url: env("URL", `http://localhost:${env.int('PORT', 1337)}/`),
  proxy: env.bool('STRAPI_PROXY', false),
  admin: {
    url: env("URL_ADMIN", `http://localhost:${env.int('PORT', 1337)}/dashboard`),
    auth: {
      autoOpen: false,
      secret: env('ADMIN_JWT_SECRET', 'f0e05fc611f6b60727c48d181022e437'),
    },
    watchIgnoreFiles: [
      "**/data/**",
    ],
  },
});

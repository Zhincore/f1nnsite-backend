module.exports = ({ env }) => ({
  defaultConnection: 'default',
  connections: {
    default: {
      connector: 'bookshelf',
      settings: (env('DATABASE_HOST', null) ?
        {
          client: 'mysql',
          host: env('DATABASE_HOST', 'localhost'),
          port: env.int('DATABASE_PORT', 3306),
          database: env('DATABASE_NAME', 'f1nnsite'),
          username: env('DATABASE_USERNAME', 'f1nnsite'),
          password: env('DATABASE_PASSWORD', 'f1nnsite'),
        }
      :
        {
          client: 'sqlite',
          filename: env('DATABASE_FILENAME', 'data/database.sqlite3'),
        }
      ),
      options: {
        useNullAsDefault: true,
      },
    },
  },
});

module.exports = ({env}) => ({
  settings: {
    xframe: {
      enabled: env("NODE_ENV") == "production",
    },
    logger: {
      level: env("STRAPI_LOG_LEVEL", "info"),
    },
  },
});
